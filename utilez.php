<?php

function usage($prog) {
  echo "Usage: " . $prog . " key(int) string [string ...]\n";
  return 1;
}

function mod_ascii($ascii) {
  if ($ascii < 32)
    $ascii += 127 - 32;
  else if ($ascii > 126)
    $ascii -= 127 - 32;
  return $ascii;
}

function cryptez($key, $str) {
  for ($i = 0; isset($str[$i]); ++$i) {
    $str[$i] = chr(mod_ascii(ord($str[$i]) + (($i % 2) ? -$key : $key)));
  }
  return $str;
}
