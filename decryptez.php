#!/usr/bin/env php
<?php

include('utilez.php');

if ($argc < 3)
  exit(usage($argv[0]));

$key = intval($argv[1]);
for ($i = 2; $i < $argc; ++$i) {
  $str = $argv[$i];
  echo cryptez(-$key, $str) . "\n";
}
